
(() => {

    const useProgramDateTime = false;

    const VODStreamUrl = ''


    // 1 create sdk instance
   const sdkInstance = new Sync.SyncSDK(useProgramDateTime)

    document.addEventListener('DOMContentLoaded', function(){
        // 2 when document loaded create sync group
        createGroup()

        initAppVod()

        // register callback for get update client list
        sdkInstance.attachListener(({ clientList }) => {
            let html = 'List of clients:'

                clientList.map(item => {
                html = html + `<div class="item-client">${item.name} - ${item.id}</div>`
            })

            document.getElementById('client-list').innerHTML = html;

        }, Sync.Callbacks.client_list_update);
    })


    async function createGroup() {
        // todo before create group you must get jwt token
        // todo code for get token can be look like this
        // const { session_jwt_token  } = await axios.get(`https://your-sharing-server/get-token?room_name=${room_name}`)
        sdkInstance.createGroup('session_jwt_token', 'testClient')
    }

    function attachPlayerToSdk (player, videoUrl) {
        const playerId = 'testPlayerId';
        const playerObject = new DemoHtmlPlayer({ id: playerId, videoUrl: videoUrl })
        playerObject.setPlayerInstance(player);
        sdkInstance.addPlayer(playerObject, Sync.PlayerType.html)
    }


    function initAppVod(){
        const playerInfo = {
            id: 'testPlayerId'
        }
        const playerInst = document.getElementById(playerInfo.id)
        attachPlayerToSdk(playerInst, VODStreamUrl)
        // register listener for play and pause button
        document.getElementById('vo-demo-play').addEventListener('click', function(){
            playerInst.play()
        })
        document.getElementById('vo-demo-pause').addEventListener('click', function(){
            playerInst.pause()
        })
    }


    // 3 create listener for start and stop sync process
    document.getElementById('start').addEventListener('click', function ()  {
        console.log('sync instance ', sdkInstance)
        sdkInstance.startSynchronize()
    })

    document.getElementById('stop').addEventListener('click',function (){
        sdkInstance.stopSynchronize()
    } )
    // remote control example
    document.getElementById('group-play').addEventListener('click', function (){
        sdkInstance.groupPlay()
    })
    document.getElementById('group-pause').addEventListener('click', function (){
        sdkInstance.groupPause()
    })
    document.getElementById('group-seek').addEventListener('click', function (){
        sdkInstance.setGroupPosition()
    })

    // simple implementation player class for Sync SDK
    class DemoHtmlPlayer extends Sync.PlayerDecorator {
        constructor({ id, videoUrl }) {
            super({ id, videoUrl });
        }

        play() {
            const playerInstance = this.getPlayerInstance();
            try {
                playerInstance?.play();
            } catch (err) {
                console.error('Error html play', err);
            }
        }
        mute() {
            const playerInstance = this.getPlayerInstance();
            try {
                playerInstance.muted = !playerInstance.muted;
            } catch (err) {
                console.error('Error html play', err);
            }
        }
        pause() {
            const playerInstance = this.getPlayerInstance();
            try {
                playerInstance.pause();
            } catch (err) {
                console.error('Error html pause', err);
            }
        }
        getCurrentPosition() {
            const playerInstance = this.getPlayerInstance();
            try {
                return playerInstance.currentTime;
            } catch (err) {
                console.error('Error get current position ', err);
                return 0;
            }
        }
        fastSeekToPosition(position) {
            if (position) {
                const playerInstance = this.getPlayerInstance();
                try {
                    const start = playerInstance.seekable.start(0);
                    const end = playerInstance.seekable.end(0);
                    if(position < end && position > start){
                        playerInstance.currentTime = position;
                    }
                } catch (err) {
                    console.error('Fast seek error: ', err);
                }
            }
        }

        isPlaying() {
            const playerInstance = this.getPlayerInstance();
            try {
                return !playerInstance.paused;
            } catch (e) {
                console.error('Error: ', e);
            }
            return false;
        }

        stop() {
            this.parrentPlayerObj.stop();
        }
        changePlaybackRate(rate) {
            const playerInstance = this.getPlayerInstance();
            try {
                playerInstance.playbackRate = rate;
            } catch (err) {
                console.error('Seek error ', err);
            }
        }
        setVolume(volume){
            const playerInstance = this.getPlayerInstance();
            try {
                playerInstance.volume = volume;
            } catch (error) {
                console.log('Change volume error: ', error);
            }
        }
    }

})()
