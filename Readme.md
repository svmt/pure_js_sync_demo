## [Synch SDK](https://documentation.sceenic.co/synchronization-sdk) 

The Synch SDK will allow you to create solution for synchronize content.

**( * ) SDKs are accessible only from the private area**

Need technical support? contact us at [Support@sceenic.co](mailto:Support@sceenic.co)


## Example description

The project is an example of integration
Synch SDK for synchronization video stream.
It contains the basic functionality.
That short tutorial help your runs project, with new auth method.

Before the start you need to have 'sceenic' account with personal credentials, 
you need to get session token from CAS server by API_SECRET and API_KEY.
You should not send request to CAS from client side. 
Instead, need to have implemented server which send request to 
CAS using API_KEY and API_SECRET and after that will share jwt token between clients.
Also, you need prepare HLS stream, which need to synchronization.

## Tutorial
### 1 Configuration
Firs step you should go to index.js file and configure project,
you need to set up 'VODStreamUrl'.
### 2 Authentication
On the second step you need to add request for get jwt token from your own sharing server using personal credential 
after that you should set up jwt token in to createGroup() method. Also, you need to use version of Synch SDK 1.0.0 and newer. 
### 3 Starting
The third step you should open index.html on a Chrome browser in two tabs and
wait when video will be loaded. Then you should press play for stars playing.
### 4 Synchronization
And the final step you should press  `` Start sync `` button on each tab to run synchronization process


## Documentation

Have a look at our official documentation site [Sceenic - WatchTogether](https://documentation.sceenic.co)

To get our sample working, go to our [Synch - tutorial](https://documentation.sceenic.co/sscale-confluence-tutorials/sscale-confluence-web/web-javascript-and-reactjs) and follow the instructions.

Read more about our SDK methods in [Synch SDK documentation](https://svmtse.atlassian.net/wiki/spaces/SC/pages/100925528/Web+SDK+reference)
